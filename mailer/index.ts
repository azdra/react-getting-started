import express from 'express';
import * as nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import {Transporter} from 'nodemailer';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const port = 9999;

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const mailerTransport = (): Transporter<SMTPTransport.SentMessageInfo> => nodemailer.createTransport({
  port: 1025,
  ignoreTLS: true,
});

app.post('/', (req, res) => {
  mailerTransport().sendMail({
    from: 'me@yolo.com',
    to: req.body.email,
    subject: 'Your computer has a virus',
    html: req.body.body,
  });
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
