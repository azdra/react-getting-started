import React from 'react';
import ReactDOM from 'react-dom';
import '@scss/index.scss';
import Router from '@app/router/Router';

ReactDOM.render(
    <React.StrictMode>
      <Router/>
    </React.StrictMode>,
    document.getElementById('root'),
);
