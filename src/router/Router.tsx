import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {routes} from '@app/router/routes';
import Home from '@screens/Home/Home';
import Movie from '@screens/Movie/Movie';
import MovieDetailScreens from '@screens/Movie/MovieDetail';

const Router = () => {
  return <BrowserRouter>
    <Switch>
      <Route exact path={routes.base} component={Home}/>
      <Route exact path={routes.movie} component={Movie}/>
      <Route exact path={routes.movieDetail} component={MovieDetailScreens}/>
    </Switch>
  </BrowserRouter>;
};

export default Router;
