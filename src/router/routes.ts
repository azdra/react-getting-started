export const routes = {
  base: '/',
  movie: '/movie',
  movieDetail: '/movie/:movie',
};
