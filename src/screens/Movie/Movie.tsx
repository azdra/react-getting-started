import React, {useEffect, useState} from 'react';
import {tmdbRequest} from '@app/api/tmdbRequest';
import Loading from '@components/Loading/Loading';
import {MovieInterface} from '@app/interfaces/MovieInterface';
import {MovieCard} from '@components/MovieCard/MovieCard';

const Movie = () => {
  const [results, setResults] = useState<MovieInterface[]|null>(null);

  useEffect(() => {
    tmdbRequest('https://api.themoviedb.org/3/trending/movie/day', 'GET').then((r) => {
      setResults(r.results);
    });
  }, []);

  const Render = (): JSX.Element => {
    return (
      <div className={'movie-wrapper-card'}>
        {
          results && results.map((movie, i) => {
            return <MovieCard key={i} {...movie}/>;
          })
        }
      </div>
    );
  };

  return results ? <Render/> : <Loading/>;
};

export default Movie;
