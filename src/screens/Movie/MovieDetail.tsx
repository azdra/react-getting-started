import React, {useEffect, useState} from 'react';
import {tmdbRequest} from '@app/api/tmdbRequest';
import Loading from '@components/Loading/Loading';
import {useParams} from 'react-router';
import {RouterParameters} from '@app/interfaces/RouterParameters';

const MovieDetailScreens = () => {
  const {movie} = useParams<RouterParameters>();
  const [results, setResults] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    tmdbRequest(`https://api.themoviedb.org/3/movie/${movie}`, 'GET').then((r) => {
      setResults(r);
    }).catch((e) => {
      setError(e);
    });
  }, []);

  const RenderError = () => {
    return (
      <div style={{display: 'flex', justifyContent: 'center', alignContent: 'center', height: '100vh', flexDirection: 'column'}}>
        <div style={{textAlign: 'center'}}>
          <div>
            <img src="https://iswenzz.com:1337/chungus.jpg" alt="BIG_CHUNGUS"/>
          </div>
          <div>
            <a style={{color: 'white'}} href="https://iswenzz.com:1337">https://iswenzz.com:1337</a>
          </div>
        </div>
      </div>
    );
  };

  const Render = () => {
    if (error) return <RenderError/>;
    return (
      <div>
        coucou
      </div>
    );
  };

  return results || (error && !results) ? <Render/> : <Loading/>;
};

export default MovieDetailScreens;
