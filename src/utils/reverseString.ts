export const reverseString = (content: string) => {
  return content.split(/\s+/).reverse().join(' ');
};
