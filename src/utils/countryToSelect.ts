import {CountryInterface} from '@app/interfaces/CountryInterface';

export const countryToSelect = (data: CountryInterface[]) => {
  const newData: { label: string; value: string; }[] = [];
  data.map((country) => {
    newData.push({label: country.name, value: country.name.toLowerCase()});
  });
  return newData;
};
