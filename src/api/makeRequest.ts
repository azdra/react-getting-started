import axios, {Method} from 'axios';

export const makeRequest = async (url: string, method: Method, data: any = {}, headers: any = {}): Promise<any> => {
  return new Promise((resolve, reject) => {
    axios({
      url,
      method,
      data,
      headers: Object.assign({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }, headers),
    })
        .then((response) => resolve(response.data))
        .catch((e) => reject(e));
  });
};
