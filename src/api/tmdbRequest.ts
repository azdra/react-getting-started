import {makeRequest} from '@app/api/makeRequest';
import {Method} from 'axios';
import {api_tmdb} from '@app/config/tmdb/api';

export const tmdbRequest = async (url: string, method: Method, data: any = {}, headers: any = {}): Promise<any> => {
  return makeRequest(url+`?api_key=${api_tmdb.key}`, method, data, headers);
};
