import {country_api_domain} from '@app/config/country/config';

export const api_country = {
  domain: country_api_domain,
  routes: {
    all: '/all',
  },
};
