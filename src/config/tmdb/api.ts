import {tmdb_api_domain, tmdb_api_key, tmdb_api_token} from '@app/config/tmdb/config';

export const api_tmdb = {
  domain: tmdb_api_domain,
  key: tmdb_api_key,
  token: tmdb_api_token,
  posterUrl: 'https://image.tmdb.org/t/p/w500/',
  routes: {
    all: '/all',
  },
};
