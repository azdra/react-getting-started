export const tmdb_api_domain = process.env.TMDB_API_URL;
export const tmdb_api_key = process.env.TMDB_API_KEY;
export const tmdb_api_token = process.env.TMDB_API_TOKEN;
