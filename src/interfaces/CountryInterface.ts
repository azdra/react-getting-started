import {CountryCurrenciesInterface} from '@app/interfaces/CountryCurrenciesInterface';
import {CountryLanguageInterface} from '@app/interfaces/CountryLanguageInterface';

export interface CountryInterface {
  alpha2Code: string;
  alpha3Code: string;
  altSpellings: string[];
  area: number;
  borders: string[];
  callingCodes: string[];
  capital: string;
  cioc: string;
  currencies: CountryCurrenciesInterface[];
  demonym: string;
  flag: {svg: string, png: string};
  independent: boolean;
  languages: CountryLanguageInterface[];
  latlng: number[];
  name: string;
  nativeName: string;
  numericCode: string;
  population: number;
  region: string;
  regionalBlocs: any;
  subregion: string;
  timezones: string[];
  topLevelDomain: string[];
  translations: any;
}
