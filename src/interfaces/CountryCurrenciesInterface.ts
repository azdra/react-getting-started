export interface CountryCurrenciesInterface {
  code: string;
  name: string;
  symbol: string;
}
