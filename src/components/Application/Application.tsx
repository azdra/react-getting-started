import React, {FC, useEffect, useState} from 'react';
import {makeRequest} from '@app/api/makeRequest';
import {api_country} from '@app/config/country/api';
import Select from 'react-select';
import {countryToSelect} from '@app/utils/countryToSelect';
import {api_mailer} from '@app/config/mailer/api';
import {SelectInterface} from '@app/interfaces/SelectInterface';
import '@scss/app.scss';
import {PageLists} from '@components/PageLists/PageLists';
import {reverseString} from '@app/utils/reverseString';

const Application: FC = () => {
  const [countries, setCountries]= useState<SelectInterface[]>();
  const [mail, setMail] = useState('');
  const [selected, setSelected] = useState<any>();
  const [emailSend, updateEmailSend] = useState(false);

  useEffect(() => {
    makeRequest(api_country.domain+api_country.routes.all, 'GET').then((r) => {
      const newData = countryToSelect(r);
      setCountries(newData);
    });
  }, []);

  const onClick = () => {
    const body = selected ? selected.map((e: SelectInterface) => e.label).join(', ') : null;
    updateEmailSend(true);
    makeRequest(api_mailer.domain, 'POST', {
      email: mail,
      body: body,
    }).then(() => updateEmailSend(false));
  };

  return (
    <div className={'app'}>
      <div className={'select'}>
        <div>
          <label htmlFor={'react-select'} className={'label color-light'}>{reverseString('Choice one or multiple')}</label>
          <Select name={'react-select'} id={'react-select'} value={selected} onChange={(e) => setSelected(e)} options={countries} isMulti />
        </div>
        <div className={'emailDiv'}>
          <label className={'label color-light'} htmlFor="email">{reverseString('Enter your e-mail')} <span className={'required'}>*</span></label>
          <input value={mail} className={'input block-center'} id={'email'} name={'email'} type="email" onChange={(e) => setMail(e.target.value)}/>
        </div>
        <div className={'text-center block-center'}>
          <button onClick={onClick} className={emailSend ? 'btn block-center btn-load' : 'btn block-center'} disabled={!(mail && selected)}>{reverseString('Envoyer')}</button>
          <PageLists/>
        </div>
      </div>
    </div>
  );
};

export default Application;
