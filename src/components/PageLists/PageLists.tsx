import React from 'react';
import {routes} from '@app/router/routes';
import {useHistory} from 'react-router';
import {reverseString} from '@app/utils/reverseString';

export const PageLists = () => {
  const router = useHistory();
  return (
    <div className={'page-btn'}>
      <h3 className={'color-light'}>{reverseString('Page lists')}</h3>
      <div className={'d-flex-center'}>
        <button onClick={() => router.push(routes.movie)} className={'btn'}>{reverseString('Film')}</button>
      </div>
    </div>
  );
};
