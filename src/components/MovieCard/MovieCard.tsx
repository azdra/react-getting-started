import React, {FC} from 'react';
import {MovieInterface} from '@app/interfaces/MovieInterface';
import './movieDetail.scss';
import {api_tmdb} from '@app/config/tmdb/api';
import {useHistory} from 'react-router';
import {routes} from '@app/router/routes';

export const MovieCard: FC<MovieInterface> = (props: MovieInterface) => {
  const router = useHistory();
  console.log(props);
  return <div onClick={() => router.push(routes.movieDetail.replace(':movie', props.id.toString()))} className={'movie-card'}>
    <img className={'movie-card-image'} src={api_tmdb.posterUrl+props.poster_path} alt=""/>
  </div>;
};
