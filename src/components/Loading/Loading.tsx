import React from 'react';
import './loading.scss';

const Loading = () => {
  return (
    <div className='loading-container'>
      <div className="loading-content">
        <div className="loading"/>
        <div className="loading-text">
          <div>
            Loading
          </div>
          <div className="loading-dots">
            <span/>
            <span/>
            <span/>
          </div>
        </div>
      </div>
    </div>

  );
};

export default Loading;
